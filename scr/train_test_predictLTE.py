__author__ = "ChinhH"
__copyright__ = "ChinhH 2020"
__version__ = "1.0.0"
__license__ = ""

import os
#import matplotlib.pyplot as plt
from core.model import Model
import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
import socket
import ast
import json
from sklearn.metrics import r2_score


def plot_results(predicted_data, true_data):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.legend()
    plt.show()

def get_test_data(data_test,seq_len, normalise):
    '''
    Create x, y test data windows
    Warning: batch method, not generative, make sure you have enough memory to
    load data, otherwise reduce size of the training split.
    '''
    data_windows = []
    for i in range(len(data_test) - seq_len):
        data_windows.append(data_test[i:i+seq_len])

    data_windows = np.array(data_windows).astype(float)
    data_source = data_windows[:,0,[0]]
    #y_test = data_windows[:, -1, [0]]
    #y_test_new = []
    #for i in range(len(y_test)):
        #y_test_new.append(y_test[i,0])
    #print(y_test_new)
    data_windows = normalise_windows(data_windows, single_window=False) if normalise else data_windows
    x = data_windows[:, :-1]
    y = data_windows[:, -1, [0]]
    return x,y,data_source

def normalise_windows(window_data, single_window=False):
    '''Normalise window with a base value of zero'''
    normalised_data = []
    window_data = [window_data] if single_window else window_data
    for window in window_data:
        normalised_window = []
        for col_i in range(window.shape[1]):
            normalised_col = [((float(p) / float(window[0, col_i])) - 1) for p in window[:, col_i]]
            normalised_window.append(normalised_col)
        normalised_window = np.array(normalised_window).T # reshape and transpose array back into original multidimensional format
        normalised_data.append(normalised_window)
    return np.array(normalised_data)

def denormalise_windows(predict,data_source):
    '''Normalise window with a base value of zero'''
    denormalise_data = []
    for i in range(len(predict)):
        denormalise_data.append((predict[i]+1)*float(data_source[i,0]))
    return denormalise_data

def train_test_model(filename):

    dataframe = pd.read_csv(filename)

    #print(dataframe)

    number_point_predic = int(bars)

    # data_test = dataframe.get(["Close","Volume"]).values[number_point_predic-1]
    data_test = dataframe.get(["Close","Open","High","Low","Volume"]).values
    data_datetime = dataframe.get(["Date"]).values
    #data_time = data_datetime[len(data_test)-51:]
    data_time = []
    for i in range(60,len(data_datetime)):
        data_time.append(data_datetime[i,0])
    
    #print("Thoi gian nen du doan: " +time_date_end)
    print("py2: Xu ly data ok")
    model = Model()
    model.load_model('saved_models/'+filename)
    x_test, y_test, data_source = get_test_data(data_test,
        seq_len=61,
        normalise=True
    )

    #Gia tri dung
    y_test_new = []
    for i in range(len(y_test)):
        y_test_new.append(y_test[i,0])
    print("py3: Load model ok")
    #Du doan dua vao datainput
    #predictions = model.predict_sequence_full(x_test, 50)
    predictions = model.predict_point_by_point(x_test)

    #Chuyen doi gia tri thuc
    pre_real = denormalise_windows(predictions,data_source)

    #Show ket qua
    plot_results(predictions,y_test_new)
    
    print("Thoi gian du doan: " + data_time[len(data_time)-1])
    print("Gia tri du doan: " + str(pre_real[len(pre_real)-1]))
    print("py4: Predict ok")
    print('============================')
    responseJSON = {}
    responseJSON['PredDate'] = data_time
    responseJSON['PredClose'] = pre_real
    responseJSON['LengPre'] = str(len(pre_real))
    return json.dumps(responseJSON) + '\r\n'

def main():
    ""


class socketserver:
    def __init__(self, address='', port=9090):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.address = address
        self.port = port
        self.sock.bind((self.address, self.port))
        self.cummdata = ''

    def recvmsg(self):
        self.sock.listen(1)
        self.conn, self.addr = self.sock.accept()
        print('connected to', self.addr)
        self.cummdata = ''

        while True:
            data = self.conn.recv(10000000)
            self.cummdata += data.decode("utf-8")
            if not data:
                break
            self.conn.send(bytes(train_test_model(self.cummdata), "utf-8"))
            return self.cummdata

    def __del__(self):
        self.sock.close()


serv = socketserver('127.0.0.1', 9090)

print('Socket Created at {}. Waiting for client..'.format(serv.sock.getsockname()))

while True:
    msg = serv.recvmsg()
