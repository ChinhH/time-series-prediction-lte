import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os.path
from os import path
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from datetime import timedelta
from datetime import datetime

# name data
# namedata = 'LTE_5212A'
namedata = 'LTE_5213A'
#namedata = 'LTE_1072A'
datatrain = 'data_train.csv'
datatest = 'data_test.csv'
timestep = 96
# read data
dataset_train = pd.read_csv(datatrain)

ps = dataset_train.columns.to_list().index(namedata)

training_set = dataset_train.iloc[:, ps:ps+1].values

lasttime = dataset_train['DateTime'][-1:]
# print(training_set)

# scale values data 0,1
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler(feature_range = (0, 1))
training_set_scaled = sc.fit_transform(training_set)

# Creat data train, X = 60 time steps, Y =  1 time step
X_train = []
y_train = []
no_of_sample = len(training_set)

for i in range(timestep, no_of_sample):
    X_train.append(training_set_scaled[i-timestep:i, 0])
    y_train.append(training_set_scaled[i, 0])

X_train, y_train = np.array(X_train), np.array(y_train)

X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))


# Build model LSTM
regressor = Sequential()
regressor.add(LSTM(units = 50, return_sequences = True, input_shape = (X_train.shape[1], 1)))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50))
regressor.add(Dropout(0.2))
regressor.add(Dense(units = 1))
regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

# if exists file model then load weight model
if path.exists(namedata+".h5"):
    regressor.load_weights(namedata+".h5")
else:
    # else make training new model
    regressor.fit(X_train, y_train, epochs = 100, batch_size = 192)
    regressor.save(namedata+".h5")

# Load data test
dataset_test = pd.read_csv(datatest)
lasttime = dataset_test['DateTime'][-1:]
real_stock_price = dataset_test.iloc[:, ps:ps+1].values

# prediction
dataset_total = pd.concat((dataset_train[namedata], dataset_test[namedata]), axis = 0)
inputs = dataset_total[len(dataset_total) - len(dataset_test) - timestep:].values
inputs = inputs.reshape(-1,1)
inputs = sc.transform(inputs)

X_test = []
no_of_sample = len(inputs)

for i in range(timestep, no_of_sample):
    X_test.append(inputs[i-timestep:i, 0])

X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
predicted_stock_price = regressor.predict(X_test)
predicted_stock_price = sc.inverse_transform(predicted_stock_price)

# pylot 
plt.plot(real_stock_price, color = 'red', label = 'Real '+ namedata)
plt.plot(predicted_stock_price, color = 'blue', label = 'Predicted ' + namedata)
plt.title(namedata+' Prediction')
plt.xlabel('Time')
plt.ylabel(namedata)
plt.legend()
plt.show()

# continue predict 


dataset_test = dataset_test[namedata][len(dataset_test)-timestep:len(dataset_test)].to_numpy()
dataset_test = np.array(dataset_test)

print(lasttime)
M = int(str(lasttime).split(".")[0].split(" ")[4])
D = int(str(lasttime).split(".")[1])
Y = int(str(lasttime).split(".")[2].split(" ")[0])
h = int(str(lasttime).split(" ")[5].split(":")[0])
m = int(str(lasttime).split(" ")[5].split(":")[1])

time_end = datetime(Y, M, D, h, m)

inputs = dataset_test
inputs = inputs.reshape(-1,1)
inputs = sc.transform(inputs)


i = 0
while i<96:
    X_test = []
    no_of_sample = len(dataset_test)

    # get last data
    X_test.append(inputs[no_of_sample - timestep:no_of_sample, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

    # predict
    predicted_stock_price = regressor.predict(X_test)

    # scale values data (0,1) to real values anh gia that
    predicted_stock_price = sc.inverse_transform(predicted_stock_price)

    # Add date time
    dataset_test = np.append(dataset_test, predicted_stock_price[0], axis=0)
    inputs = dataset_test
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    newtime = time_end + timedelta(minutes=15*(i+1))
    print('data '+ namedata +" "+ str(newtime) + ' : ', predicted_stock_price[0][0])
    i = i +1

